namespace AlloyTrainingTempCMS88AddOns.Business.Rendering
{
    /// <summary>
    /// Marker interface for content types which should not be handled by DefaultPageController.
    /// </summary>
    interface IContainerPage
    {
    }
}
