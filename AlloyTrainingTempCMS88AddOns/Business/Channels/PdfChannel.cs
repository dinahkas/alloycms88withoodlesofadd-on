﻿using EPiServer.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlloyTrainingTempCMS88AddOns.Business.Channels
{
    public class PdfChannel : DisplayChannel
    {
        public override string ChannelName
        {
            get { return "PDF"; }
        }

        public override bool IsActive(HttpContextBase context)
        {
            if (context == null)
            {
                return false;
            }

            return context.Request.QueryString["pdf"] == 1.ToString();
        }
    }
}