using System;
using System.ComponentModel.DataAnnotations;
using AlloyTrainingTempCMS88AddOns.Models.Blocks;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;


namespace AlloyTrainingTempCMS88AddOns.Models.Pages
{
    /// <summary>
    /// Used for the pages mainly consisting of manually created content such as text, images, and blocks
    /// </summary>
    [SiteContentType(GUID = "9CCC8A41-5C8C-4BE0-8E73-520FF3DE8267")]
    [SiteImageUrl(Global.StaticGraphicsFolderPath + "page-type-thumbnail-standard.png")]
    public class StandardPage : SitePageData
    {
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 310)]
        [CultureSpecific]
        public virtual XhtmlString MainBody { get; set; }

        //[Display(Prompt ="Share it to social networks",
        //    GroupName = SystemTabNames.Content,
        //    Order = 320)]
        //[CultureSpecific]
        //public virtual ShareIt ShareIt { get; set; }

        [Display(Prompt = "Google Maps",
            GroupName = SystemTabNames.Content,
            Order = 320)]
        [CultureSpecific]
        [EditorDescriptor(EditorDescriptorType = typeof(CoordinatesEditorDescriptor))]
        public virtual string Coordinates { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Order = 330)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}
