using EPiServer.Core;

namespace AlloyTrainingTempCMS88AddOns.Models.Pages
{
    public interface IHasRelatedContent
    {
        ContentArea RelatedContentArea { get; }
    }
}
