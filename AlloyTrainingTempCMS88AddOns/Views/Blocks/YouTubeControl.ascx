<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="YouTubeControl.ascx.cs" Inherits="EPiServer.Templates.Alloy.Views.Blocks.YouTubeControl" %>

<asp:PlaceHolder ID="VideoPlaceHolder" runat="server" Visible="false">
    <div>
        <div class="media">
            <EPiServer:Property ID="Heading" PropertyName="Heading" CustomTagName="h2" runat="server"/>
            <EPiServer:Property ID="VideoText" PropertyName="VideoText" runat="server" />
        </div>
        <div class="embed" id="embed" runat="server">
            <div style="position:absolute; width: 100%; height: 100%">
                    <param name='movie' value='<%# CurrentBlock.YouTubeLink %>' />
                    <param name='wmode' value='transparent'/>
                    <param name="autoplay" value="false" />
                    <param name="autoload" value="false" />
                    <embed src='<%# CurrentBlock.YouTubeLink %>' 
                        type='application/x-shockwave-flash' wmode='transparent' >
                    </embed>
            </div>
        </div>
    </div>
 </asp:PlaceHolder>