using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web;
using EPiServer.Web.WebControls;
using AlloyTrainingTempCMS88AddOns.Models;
using AlloyTrainingTempCMS88AddOns;

namespace AlloyTrainingTempCMS88AddOns.Models.Blocks
{
    public partial class YouTubeControl : SiteBlockControlBase<YouTubeBlock>
    {
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            if (CurrentBlock.HasVideo)
            {
                VideoPlaceHolder.Visible = true;

                DataBind();
            }
        }
    }
}