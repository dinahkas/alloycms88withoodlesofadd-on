﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using System.Text;
using AlloyTrainingTempCMS88AddOns.Models.Pages;
using AlloyTraining.Business.Channels;
using AlloyTrainingTempCMS88AddOns.Models.ViewModels;
//using Rotativa;

namespace AlloyTrainingTempCMS88AddOns.Controllers
{

    [TemplateDescriptor(Inherited = true, Tags = new[] { "pdf" })]
    public class PdfController : PageController<Models.Pages.ProductPage>
    {
        public ActionResult Index(ProductPage currentPage)
        {
            //Create HTML to send to PDF
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<{0}>{1}</{0}>", "h1", currentPage.Name);
            sb.AppendFormat("<{0}>{1}</{0}>", "h3", currentPage.MetaDescription);
            sb.Append(currentPage.MainBody.ToString());
            //generate the PDF
            PDFChannelHelper.GeneratePDF(sb.ToString());
            PageViewModel<ProductPage> model = new PageViewModel<ProductPage>(currentPage);

            return View(model);

        }
    }
}